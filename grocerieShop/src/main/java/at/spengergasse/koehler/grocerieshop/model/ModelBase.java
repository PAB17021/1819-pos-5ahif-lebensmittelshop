package at.spengergasse.koehler.grocerieshop.model;

public class ModelBase {
    private Long id;
    private Long version;

    public boolean isNew() {
        return id == null;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }
}
