package at.spengergasse.koehler.grocerieshop.service;

import at.spengergasse.koehler.grocerieshop.model.Orders;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Service
@FeignClient(name = "orders", path = "/api/orders")
public interface OrderService {
    @GetMapping
    public List<Orders> getOrders();

    @GetMapping(path = "{id]")
    public Orders getOrdersById(Long id);

    @PostMapping()
    public Orders saveOrders(Orders orders);
}
