package at.spengergasse.koehler.grocerieshop.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;


public class Orders extends ModelBase {

    private int orderID; // the ID of the current order
    private int customerID; // the ID of the ordering customer
    private String address; // the address of the order
    private List<Groceries2> orderedArticles; // a list of the Article ID's
    private LocalDateTime orderTime; // the date and timestamp of the order;

    public Orders() {

    }

    public Orders(int orderID, int customerID, String address, List<Groceries2> orderedArticles, LocalDateTime orderTime) {
        this.orderID = orderID;
        this.customerID = customerID;
        this.address = address;
        this.orderedArticles = orderedArticles;
        this.orderTime = orderTime;
    }

    public int getOrderID() {
        return orderID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public String getAddress() {
        return address;
    }

    public static OrdersBuilder builder(Orders orders) {
        return new OrdersBuilder().customerID(orders.getCustomerID()).address(orders.getAddress()).orderID(orders.getOrderID()).orderTime(orders.getOrderTime());
    }

    public LocalDateTime getOrderTime() {
        return orderTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Orders orders = (Orders) o;
        return orderID == orders.orderID &&
                customerID == orders.customerID &&
                Objects.equals(address, orders.address) &&
                Objects.equals(orderedArticles, orders.orderedArticles) &&
                Objects.equals(orderTime, orders.orderTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderID, customerID, address, orderedArticles, orderTime);
    }

    @Override
    public String toString() {
        return "Orders{" +
                "orderID=" + orderID +
                ", customerID=" + customerID +
                ", address='" + address + '\'' +
                ", orderedArticles=" + orderedArticles +
                ", orderTime=" + orderTime +
                '}';
    }

    public static OrdersBuilder builder() {
        return new OrdersBuilder();
    }

    public List<Groceries2> getOrderedArticles() {
        return orderedArticles;
    }

    public static final class OrdersBuilder {
        private int orderID;
        private int customerID;
        private String address;
        private List<Groceries2> orderedArticles;
        private LocalDateTime orderTime;

        private OrdersBuilder() {

        }

        public OrdersBuilder orderID(int orderID) {
            this.orderID = orderID;
            return this;
        }

        public OrdersBuilder customerID(int customerID) {
            this.customerID = customerID;
            return this;
        }

        public OrdersBuilder address(String address) {
            this.address = address;
            return this;
        }

        public OrdersBuilder orderedArticles(List<Groceries2> orderedArticles) {
            this.orderedArticles = orderedArticles;
            return this;
        }

        public OrdersBuilder orderTime(LocalDateTime orderTime) {
            this.orderTime = orderTime;
            return this;
        }

        public Orders build() {
            return new Orders(this.orderID, this.customerID, this.address, this.orderedArticles, this.orderTime);
        }
    }
}
