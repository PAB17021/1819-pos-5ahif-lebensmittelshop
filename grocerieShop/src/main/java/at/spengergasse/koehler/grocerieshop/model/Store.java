package at.spengergasse.koehler.grocerieshop.model;

public class Store {
    private String name;
    private String welcomeMessage;
    private String motto;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWelcomeMessage() {
        return welcomeMessage;
    }

    public void setWelcomeMessage(String welcomeMessage) {
        this.welcomeMessage = welcomeMessage;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }


    public static final class GroceriesStoreBuilder {
        private String name;
        private String welcomeMessage;
        private String motto;

        private GroceriesStoreBuilder() {

        }

        public GroceriesStoreBuilder name(String name) {
            this.name = name;
            return this;
        }

        public GroceriesStoreBuilder welcomeMessage(String welcomeMessage) {
            this.welcomeMessage = welcomeMessage;
            return this;
        }

        public GroceriesStoreBuilder motto(String motto) {
            this.motto = motto;
            return this;
        }

        public Store build() {
            Store store = new Store();
            store.name = this.name;
            store.motto = this.motto;
            store.welcomeMessage = this.welcomeMessage;
            return store;
        }
    }
}