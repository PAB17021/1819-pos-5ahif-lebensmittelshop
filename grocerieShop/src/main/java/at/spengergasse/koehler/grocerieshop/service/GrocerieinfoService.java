package at.spengergasse.koehler.grocerieshop.service;

import at.spengergasse.koehler.grocerieshop.model.Groceries;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Optional;

@Service
@FeignClient(name = "groceries")
public interface GrocerieinfoService {
    @GetMapping(path = "/api/groceries")
    public Optional<List<Groceries>> getGroceries();

    @RequestMapping(method = RequestMethod.GET, path = "/api/groceries/{id}")
    public Optional<Groceries> getGroceriesById(@PathVariable("id") long id);

}
