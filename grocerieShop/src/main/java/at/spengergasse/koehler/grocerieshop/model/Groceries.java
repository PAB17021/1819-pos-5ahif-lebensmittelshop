package at.spengergasse.koehler.grocerieshop.model;

import java.time.LocalDateTime;

public class Groceries extends ModelBase {
    private String name;
    private int priceInCents;
    private int stock;
    private int weight;
    private LocalDateTime preservableFrom;
    private LocalDateTime preservableTo;
    private String contents;

    public Groceries() {
    }

    public Groceries(String name, int priceInCents, int stock, int weight, LocalDateTime preservableFrom, LocalDateTime preservableTo, String contents) {
        this.name = name;
        this.priceInCents = priceInCents;
        this.stock = stock;
        this.weight = weight;
        this.preservableFrom = preservableFrom;
        this.preservableTo = preservableTo;
        this.contents = contents;
    }

    public double whichPriceInEuros() {
        return (((double) this.priceInCents) / 100);
    }

    public boolean stillPreservable(LocalDateTime dateTime) {
        return preservableTo.isBefore(dateTime);
    }

    public String getName() {
        return name;
    }

    public int getPriceInCents() {
        return priceInCents;
    }

    public int getStock() {
        return stock;
    }

    public int getWeight() {
        return weight;
    }

    public LocalDateTime getPreservableFrom() {
        return preservableFrom;
    }

    public LocalDateTime getPreservableTo() {
        return preservableTo;
    }

    public String getContents() {
        return contents;
    }

    public static GroceriesBuilder builder() {
        return new GroceriesBuilder();
    }

    public static final class GroceriesBuilder {
        private String name;
        private int priceInCents;
        private int stock;
        private int weight;
        private LocalDateTime preservableFrom;
        private LocalDateTime preservableTo;
        private String contents;

        private GroceriesBuilder() {
        }

        public GroceriesBuilder name(String name) {
            this.name = name;
            return this;
        }

        public GroceriesBuilder priceInCents(int priceInCents) {
            this.priceInCents = priceInCents;
            return this;
        }

        public GroceriesBuilder stock(int stock) {
            this.stock = stock;
            return this;
        }

        public GroceriesBuilder weight(int weight) {
            this.weight = weight;
            return this;
        }

        public GroceriesBuilder preservableFrom(LocalDateTime preservableFrom) {
            this.preservableFrom = preservableFrom;
            return this;
        }

        public GroceriesBuilder preservableTo(LocalDateTime preservableTo) {
            this.preservableTo = preservableTo;
            return this;
        }

        public GroceriesBuilder contents(String contents) {
            this.contents = contents;
            return this;
        }

        public Groceries build() {
            return new Groceries(this.name, this.priceInCents, this.stock, this.weight, this.preservableFrom, this.preservableTo, this.contents);
        }
    }
}

