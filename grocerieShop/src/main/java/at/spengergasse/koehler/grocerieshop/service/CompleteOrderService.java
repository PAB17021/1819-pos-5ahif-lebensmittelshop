package at.spengergasse.koehler.grocerieshop.service;

import at.spengergasse.koehler.grocerieshop.model.CompleteOrders;
import at.spengergasse.koehler.grocerieshop.model.Groceries;
import at.spengergasse.koehler.grocerieshop.model.Groceries2;
import at.spengergasse.koehler.grocerieshop.model.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CompleteOrderService {

    private GrocerieinfoService grocerieinfoService;


    public CompleteOrderService(@Autowired GrocerieinfoService grocerieinfoService) {
        this.grocerieinfoService = grocerieinfoService;
    }

    public List<CompleteOrders> getCompleteOrders(List<Orders> orders) {
        List<CompleteOrders> completeOrders = new ArrayList<>();
        for (Orders order : orders) {
            List<Groceries> groceriesList = new ArrayList<>();
            for (Groceries2 groceries : order.getOrderedArticles()) {
                Optional<Groceries> groceriesById = grocerieinfoService.getGroceriesById(groceries.getId());
                groceriesById.ifPresent(groceries1 -> groceriesList.add(groceries1));
            }
            completeOrders.add(CompleteOrders.builder(order).orderedArticles(groceriesList).build());
        }
        return completeOrders;
    }

    public List<CompleteOrders> getCompleteOrdersRandom(List<Orders> orders) {
        List<CompleteOrders> completeOrders = new ArrayList<>();
        List<Groceries> groceriesList = new ArrayList<>();
        groceriesList = grocerieinfoService.getGroceries().get();
        completeOrders.add(CompleteOrders.builder(orders.get(0)).orderedArticles(groceriesList).build());
        return completeOrders;
    }
}
