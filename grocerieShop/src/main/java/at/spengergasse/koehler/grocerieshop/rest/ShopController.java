package at.spengergasse.koehler.grocerieshop.rest;

import at.spengergasse.koehler.grocerieshop.model.CompleteOrders;
import at.spengergasse.koehler.grocerieshop.service.CompleteOrderService;
import at.spengergasse.koehler.grocerieshop.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController("/api/shop")
public class ShopController {

    private CompleteOrderService completeOrderService;
    private OrderService orderService;

    @Autowired
    public ShopController(CompleteOrderService completeOrderService, OrderService orderService) {
        this.completeOrderService = completeOrderService;
        this.orderService = orderService;

    }

    @GetMapping
    public ResponseEntity<List<CompleteOrders>> getAllOrders() {
        return ResponseEntity.ok(completeOrderService.getCompleteOrders(orderService.getOrders()));
    }

    @GetMapping(value = "random")
    public ResponseEntity<List<CompleteOrders>> getAllOrdersRandom() {
        return ResponseEntity.ok(completeOrderService.getCompleteOrdersRandom(orderService.getOrders()));
    }
}
