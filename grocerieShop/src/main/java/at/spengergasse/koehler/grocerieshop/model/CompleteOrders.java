package at.spengergasse.koehler.grocerieshop.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;


public class CompleteOrders extends ModelBase {

    private int orderID; // the ID of the current order
    private int customerID; // the ID of the ordering customer
    private String address; // the address of the order
    private List<Groceries> orderedArticles; // a list of the Article ID's
    private LocalDateTime orderTime; // the date and timestamp of the order;

    public CompleteOrders() {

    }

    public CompleteOrders(int orderID, int customerID, String address, List<Groceries> orderedArticles, LocalDateTime orderTime) {
        this.orderID = orderID;
        this.customerID = customerID;
        this.address = address;
        this.orderedArticles = orderedArticles;
        this.orderTime = orderTime;
    }

    public static CompleteOrdersBuilder builder() {
        return new CompleteOrdersBuilder();
    }

    public static CompleteOrdersBuilder builder(Orders orders) {
        return new CompleteOrdersBuilder().customerID(orders.getCustomerID()).address(orders.getAddress()).orderID(orders.getOrderID()).orderTime(orders.getOrderTime());
    }

    public int getOrderID() {
        return orderID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public String getAddress() {
        return address;
    }

    public List<Groceries> getOrderedArticles() {
        return orderedArticles;
    }

    public LocalDateTime getOrderTime() {
        return orderTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompleteOrders orders = (CompleteOrders) o;
        return orderID == orders.orderID &&
                customerID == orders.customerID &&
                Objects.equals(address, orders.address) &&
                Objects.equals(orderedArticles, orders.orderedArticles) &&
                Objects.equals(orderTime, orders.orderTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderID, customerID, address, orderedArticles, orderTime);
    }

    @Override
    public String toString() {
        return "CompleteOrders{" +
                "orderID=" + orderID +
                ", customerID=" + customerID +
                ", address='" + address + '\'' +
                ", orderedArticles=" + orderedArticles +
                ", orderTime=" + orderTime +
                '}';
    }

    public static final class CompleteOrdersBuilder {
        private int orderID;
        private int customerID;
        private String address;
        private List<Groceries> orderedArticles;
        private LocalDateTime orderTime;

        private CompleteOrdersBuilder() {

        }

        public CompleteOrdersBuilder orderID(int orderID) {
            this.orderID = orderID;
            return this;
        }

        public CompleteOrdersBuilder customerID(int customerID) {
            this.customerID = customerID;
            return this;
        }

        public CompleteOrdersBuilder address(String address) {
            this.address = address;
            return this;
        }

        public CompleteOrdersBuilder orderedArticles(List<Groceries> orderedArticles) {
            this.orderedArticles = orderedArticles;
            return this;
        }

        public CompleteOrdersBuilder orderTime(LocalDateTime orderTime) {
            this.orderTime = orderTime;
            return this;
        }

        public CompleteOrders build() {
            return new CompleteOrders(this.orderID, this.customerID, this.address, this.orderedArticles, this.orderTime);
        }
    }
}
