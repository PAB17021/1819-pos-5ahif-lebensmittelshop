package at.spengergasse.brunner.grocerieinfo.service;

import at.spengergasse.brunner.grocerieinfo.model.Groceries;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class GroceriesExampleData {

    public static List<Groceries> getGroceriesExample() {
        List<Groceries> groceriesList = new ArrayList<>();

        Groceries groceries1 = Groceries.builder().name("Tomato").contents("sugar oh sugar").priceInCents(230).weight(23).stock(100).preservableFrom(LocalDateTime.of(2018, 12, 20, 10, 10)).preservableTo(LocalDateTime.of(2018, 12, 30, 20, 10)).build();
        Groceries groceries2 = Groceries.builder().name("Potato").contents("strength").priceInCents(120).weight(50).stock(76).preservableFrom(LocalDateTime.of(2018, 12, 13, 11, 32)).preservableTo(LocalDateTime.of(2019, 1, 19, 22, 10)).build();
        Groceries groceries3 = Groceries.builder().name("Flamenco").contents("cost").priceInCents(13).weight(1).stock(35).preservableFrom(LocalDateTime.of(2018, 12, 23, 10, 10)).preservableTo(LocalDateTime.of(2019, 1, 25, 20, 10)).build();

        groceriesList.add(groceries1);
        groceriesList.add(groceries2);
        groceriesList.add(groceries3);
        return groceriesList;
    }
}
