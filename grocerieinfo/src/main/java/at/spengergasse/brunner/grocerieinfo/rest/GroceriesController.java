package at.spengergasse.brunner.grocerieinfo.rest;

import at.spengergasse.brunner.grocerieinfo.model.Groceries;
import at.spengergasse.brunner.grocerieinfo.service.GroceriesExampleData;
import at.spengergasse.brunner.grocerieinfo.service.GroceriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/groceries")
public class GroceriesController {

    private GroceriesService groceriesService;

    @Autowired
    public GroceriesController(GroceriesService groceriesService) {
        this.groceriesService = groceriesService;
        groceriesService.saveGroceries(GroceriesExampleData.getGroceriesExample());
    }

    @GetMapping
    @ResponseBody
    public ResponseEntity<List<Groceries>> getAllGroceries() {
        return ResponseEntity.ok(groceriesService.getAllGroceries());
    }

    @GetMapping("{id}")
    @ResponseBody
    public ResponseEntity<Groceries> getGroceries(@PathVariable Long id) {
        return ResponseEntity.ok(groceriesService.getGroceriesById(id));
    }

    @PostMapping
    public ResponseEntity saveGroceries(@RequestBody Groceries groceries) {
        groceriesService.saveGroceries(groceries);
        return ResponseEntity.created(URI.create("/api/groceries/" + groceries.getId())).body(groceries);
    }

    @DeleteMapping
    public ResponseEntity<Groceries> deleteGroceries(Groceries groceries) {
        groceriesService.deleteGroceries(groceries);
        return ResponseEntity.ok(groceries);
    }
}
