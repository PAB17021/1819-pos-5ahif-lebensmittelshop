package at.spengergasse.brunner.grocerieinfo.service;

import at.spengergasse.brunner.grocerieinfo.model.Groceries;
import at.spengergasse.brunner.grocerieinfo.persistence.DaoGroceries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GroceriesService {

    private DaoGroceries daoGroceries;

    @Autowired
    public GroceriesService(DaoGroceries daoGroceries) {
        this.daoGroceries = daoGroceries;
    }

    private DaoGroceries groceries;

    public void saveGroceries(Groceries groceries) {
        daoGroceries.save(groceries);
    }

    public void saveGroceries(List<Groceries> groceriesList) {
        daoGroceries.saveAll(groceriesList);
    }

    public List<Groceries> getAllGroceries() {
        List<Groceries> groceriesList = new ArrayList<>();
        daoGroceries.findAll().forEach(groceriesList::add);
        return groceriesList;
    }

    public Groceries getGroceriesById(Long id) {
        Optional<Groceries> groceries = daoGroceries.findById(id);
        return groceries.orElse(null);
    }

    public List<Groceries> getGroceriesById(List<Long> ids) {
        Optional<List<Groceries>> groceries = daoGroceries.findAllById(ids);
        return groceries.orElse(new ArrayList<>());
    }

    public List<Groceries> getExpiredGroceries(LocalDateTime localDateTime) {
        List<Groceries> groceriesList = new ArrayList<>();
        daoGroceries.findAll().forEach(groceries1 -> {
            if (groceries1.getPreservableTo().isBefore(localDateTime)) {
                groceriesList.add(groceries1);
            }
        });
        return groceriesList;
    }

    public Groceries deleteGroceries(Groceries groceries) {
        daoGroceries.delete(groceries);
        return groceries;
    }
}
