package at.spengergasse.brunner.grocerieinfo.service;

import at.spengergasse.brunner.grocerieinfo.persistence.DaoGroceries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration {

    private DaoGroceries daoGroceries;

    @Autowired
    public ServiceConfiguration(DaoGroceries daoGroceries) {
        this.daoGroceries = daoGroceries;
    }

    @Bean
    public GroceriesService groceriesService() {
        return new GroceriesService(daoGroceries);
    }
}
