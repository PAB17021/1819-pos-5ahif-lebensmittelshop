package at.spengergasse.brunner.grocerieinfo.persistence;

import at.spengergasse.brunner.grocerieinfo.model.Groceries;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DaoGroceries extends CrudRepository<Groceries, Long> {

    Optional<List<Groceries>> findAllById(List<Long> ids);
}
