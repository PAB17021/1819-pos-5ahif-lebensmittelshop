package at.spengergasse.brunner.grocerieinfo.model;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;

import static org.assertj.core.api.Java6Assertions.assertThat;

@ExtendWith(SpringExtension.class)
public class GroceriesTest {

    @Test
    public void initializationTest() {
        Groceries groceries = Groceries.builder().name("Tomato")
                .contents("tomato, water")
                .stock(23)
                .weight(100)
                .priceInCents(1300)
                .preservableFrom(LocalDateTime.MIN)
                .preservableTo(LocalDateTime.MAX)
                .build();
        assertThat(groceries.getName()).isEqualTo("Tomato");
        assertThat(groceries.getStock()).isEqualTo(23);
        assertThat(groceries.getPriceInCents()).isEqualTo(1300);
        assertThat(groceries.getWeight()).isEqualTo(100);
        assertThat(groceries.getContents()).contains("to, wat");
        assertThat(groceries.getPreservableFrom()).isEqualTo(LocalDateTime.MIN);
        assertThat(groceries.getPreservableTo()).isEqualTo(LocalDateTime.MAX);
    }
}
