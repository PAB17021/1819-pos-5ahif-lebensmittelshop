package at.spengergasse.pable.ordersystem.rest;

import at.spengergasse.pable.ordersystem.model.Orders;
import at.spengergasse.pable.ordersystem.service.OrderExampleData;
import at.spengergasse.pable.ordersystem.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/api/orders")
public class OrdersController {

    private OrdersService ordersService;

    @Autowired
    public OrdersController(OrdersService ordersService) {
        this.ordersService = ordersService;
        ordersService.saveOrders(OrderExampleData.getOrdersExample());
    }

    @GetMapping
    public ResponseEntity<List<Orders>> getAllOrders() {
        return ResponseEntity.ok(ordersService.getAllOrders());
    }

    @GetMapping(path = "{id}")
    @ResponseBody
    public ResponseEntity<Orders> getOrdersById(@PathVariable Long id) {
        return ResponseEntity.ok(ordersService.getOrdersById(id));
    }

    @PostMapping
    public ResponseEntity saveOrders(@RequestBody Orders orders) {
        ordersService.saveOrders(orders);
        return ResponseEntity.created(URI.create("/api/orders/" + orders.getId())).body(orders);
    }

    @DeleteMapping
    public ResponseEntity<Orders> deleteOrders(Orders orders) {
        ordersService.deleteOrders(orders);
        return ResponseEntity.ok(orders);
    }
}
