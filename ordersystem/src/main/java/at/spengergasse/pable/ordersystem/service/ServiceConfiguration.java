package at.spengergasse.pable.ordersystem.service;

import at.spengergasse.pable.ordersystem.persistance.DaoOrders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration {

    private DaoOrders daoOrders;

    @Autowired
    public ServiceConfiguration(DaoOrders daoOrders) {
        this.daoOrders = daoOrders;
    }

    @Bean
    public OrdersService ordersService() {
        return new OrdersService(daoOrders);
    }
}
