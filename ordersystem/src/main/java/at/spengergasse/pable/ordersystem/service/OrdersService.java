package at.spengergasse.pable.ordersystem.service;

import at.spengergasse.pable.ordersystem.model.Orders;
import at.spengergasse.pable.ordersystem.persistance.DaoOrders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrdersService {

    private DaoOrders daoOrders;

    @Autowired
    public OrdersService(DaoOrders daoOrders) {
        this.daoOrders = daoOrders;
    }

    public void saveOrders(Orders orders) {
        daoOrders.save(orders);
    }

    public void saveOrders(List<Orders> groceriesList) {
        daoOrders.saveAll(groceriesList);
    }

    public List<Orders> getAllOrders() {
        List<Orders> groceriesList = new ArrayList<>();
        daoOrders.findAll().forEach(groceriesList::add);
        return groceriesList;
    }

    public Orders getOrdersById(Long id) {
        Optional<Orders> orders = daoOrders.findById(id);
        return orders.orElse(null);
    }

    public List<Orders> getIsToLate(LocalDateTime localDateTime) {
        List<Orders> groceriesList = new ArrayList<>();
        daoOrders.findAll().forEach(groceries1 -> {
            if (groceries1.getOrderTime().isBefore(localDateTime)) {
                groceriesList.add(groceries1);
            }
        });
        return groceriesList;
    }

    public Orders deleteOrders(Orders orders) {
        daoOrders.delete(orders);
        return orders;
    }
}
