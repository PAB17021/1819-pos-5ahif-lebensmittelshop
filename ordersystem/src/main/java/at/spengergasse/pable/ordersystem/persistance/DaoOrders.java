package at.spengergasse.pable.ordersystem.persistance;

import at.spengergasse.pable.ordersystem.model.Orders;
import org.springframework.data.repository.CrudRepository;

public interface DaoOrders extends CrudRepository<Orders, Long> {
}
