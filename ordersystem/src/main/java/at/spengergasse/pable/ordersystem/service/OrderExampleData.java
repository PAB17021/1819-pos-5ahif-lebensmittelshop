package at.spengergasse.pable.ordersystem.service;

import at.spengergasse.pable.ordersystem.model.Groceries;
import at.spengergasse.pable.ordersystem.model.Orders;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class OrderExampleData {

    public static List<Orders> getOrdersExample() {
        List<Orders> orders = new ArrayList<>();

        Groceries groceries1 = new Groceries();
        groceries1.setId(1l);
        Groceries groceries2 = new Groceries();
        groceries2.setId(2l);
        Groceries groceries3 = new Groceries();
        groceries3.setId(3l);
        List<Groceries> groceriesList1 = new ArrayList<>();
        groceriesList1.add(groceries1);
        groceriesList1.add(groceries2);
        List<Groceries> groceriesList2 = new ArrayList<>();
        groceriesList2.add(groceries3);

        orders.add(Orders.builder().orderID(23).customerID(244).orderTime(LocalDateTime.of(2018, 12, 20, 23, 3)).address("Hellostreet").orderedArticles(groceriesList1).build());
        orders.add(Orders.builder().orderID(56).customerID(65).orderTime(LocalDateTime.of(2018, 11, 30, 23, 3)).address("Byestreet").orderedArticles(groceriesList2).build());
        return orders;
    }
}
